import { configure, getConsoleSink, getLogger, Logger, LogLevel } from "@logtape/logtape";

export interface LoggerConfig {
    level: LogLevel;
}

export async function initLogger(config: LoggerConfig): Promise<Logger> {
  await configure({
    sinks: { console: getConsoleSink() },
    loggers: [
      { category: "TootFeedBot", lowestLevel: config.level, sinks: ["console"] },
      // See https://logtape.org/manual/categories#meta-logger
      { category: ["logtape", "meta"], lowestLevel: "error", sinks: ["console"] },
    ]
  });

  return getLogger(["TootFeedBot"]);
}
