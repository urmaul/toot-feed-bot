import { Logger } from "@logtape/logtape";
import Keyv from 'keyv';
import { Crypto } from './crypto.ts';
import { RoomId } from './types.ts';
import { Subscription } from './subscription.ts';
import { FediverseConfig } from './fediverse.ts';
import KeyvSqlite from '@keyv/sqlite';
import { OngoingRegistration } from './registration.ts';
import { hasUncaughtExceptionCaptureCallback } from "node:process";

// Data store

export interface StoreConfig {
    // Connection line for the Keyv store
    // See: https://github.com/jaredwray/keyv#storage-adapters
    // So far, only sqlite is supported
    uri: string | undefined;
    // Encryption secret
    secret: string;
}

export class Store {
    private keyv: Keyv;
    private subscriptions: Keyv;
    private crypto: Crypto;
    private logger: Logger;

    fediverseConfigs: FediverseConfigRepository;
    maxStatusIds: MaxIdRepository;
    maxNotificationIds: MaxIdRepository;

    constructor(config: StoreConfig, logger: Logger) {
        logger.debug(`Initializing store at ${config.uri}`);

        this.crypto = new Crypto(config.secret);
        // deno-lint-ignore no-explicit-any
        const serialize = (data: any) => this.crypto.encrypt(JSON.stringify(data));
        // deno-lint-ignore no-explicit-any
        const deserialize = (text: any) => JSON.parse(this.crypto.decrypt(text));
        const store = new KeyvSqlite({ uri: config.uri });

        this.keyv = new Keyv({ store, serialize, deserialize, namespace: 'bot' });
        this.keyv.on('error', error => logger.error('Store Error', { error }));
        this.subscriptions = new Keyv({ store, serialize, deserialize, namespace: 'subscription' });
        this.subscriptions.on('error', error => logger.error('Subscriptions store Error', { error }));
        this.logger = logger;

        const fediverseConfigKey = (hostname: string) => `fediverseConfig:${hostname}`;
        const maxStatusIdKey = (roomId: RoomId) => `maxStatusId:${this.hash(roomId)}`;
        const maxNotificationIdKey = (roomId: RoomId) => `maxNotificationId:${this.hash(roomId)}`;

        this.fediverseConfigs = {
            get: (hostname: string): Promise<FediverseConfig | undefined> =>
                this.tryGet(fediverseConfigKey(hostname)),
            add: (fediverseConfig: FediverseConfig): Promise<void> => 
                this.trySet(fediverseConfigKey(fediverseConfig.ref.hostname), fediverseConfig),
        };

        this.maxStatusIds = {
            get: (roomId: RoomId): Promise<string | undefined> =>
                this.tryGet(maxStatusIdKey(roomId)),
            set: (roomId: RoomId, newValue: string): Promise<void> =>
                this.trySet(maxStatusIdKey(roomId), newValue),
            delete: (roomId: RoomId): Promise<void> =>
                this.tryDelete(maxStatusIdKey(roomId)),
        };

        this.maxNotificationIds = {
            get: (roomId: RoomId): Promise<string | undefined> =>
                this.tryGet(maxNotificationIdKey(roomId)),
            set: (roomId: RoomId, newValue: string): Promise<void> =>
                this.trySet(maxNotificationIdKey(roomId), newValue),
            delete: (roomId: RoomId): Promise<void> =>
                this.tryDelete(maxNotificationIdKey(roomId)),
        };
    }

    private hash(roomId: RoomId): string {
        return this.crypto.hash(roomId.value);
    }

    // deno-lint-ignore no-explicit-any
    private async tryGet(key: string): Promise<any | undefined> {
        try {
            return await this.keyv.get(key);
        } catch (error) {
            this.logger.error(`Error while getting ${key}: ${error}`);
            this.logger.error(`Error while getting ${key}`, { error });
            // Fallback to undefined
            return undefined;
        }
    }

    // deno-lint-ignore no-explicit-any
    private async trySet(key: string, value: any): Promise<void> {
        try {
            await this.keyv.set(key, value);
        } catch (error) {
            this.logger.error(`Error while setting ${key}`, { error });
        }
    }

    private async tryDelete(key: string): Promise<void> {
        try {
            await this.keyv.delete(key);
        } catch (error) {
            this.logger.error(`Error while deleting ${key}`, { error });
        }
    }

    async healthCheck(): Promise<void> {
        const key = "healthCheck"
        const value = Date.now().toString()
        await this.trySet(key, value);
        const actual = await this.tryGet(key);
        this.logger.debug(`healthCheck Expected: ${value} Actual: ${actual}`);
        if (actual != value) {
            const message = `Store does not work. Expected: ${value} Actual: ${actual}`;
            this.logger.error(message);
            throw message;
        }

        await this.tryDelete(key);
    }

    // -- Subscriptions --

    async addSubscription(subscription: Subscription): Promise<void> {
        await this.subscriptions.set(this.hash(subscription.roomId), subscription);
    }

    async getAllSubscriptions(): Promise<Subscription[]> {
        const subscriptions: Subscription[] = [];
        /* eslint-disable @typescript-eslint/no-unused-vars */
        for await (const [_, subscription] of this.subscriptions.iterator()) {
            subscriptions.push(subscription);
        }
        /* eslint-enable @typescript-eslint/no-unused-vars */
        return subscriptions;
    }

    async getSubscription(roomId: RoomId): Promise<Subscription | undefined> {
        try {
            return await this.subscriptions.get(this.hash(roomId));
        } catch (error) {
            this.logger.error(`Error while getting subscription for ${this.hash(roomId)}`, { error });
            // Fallback to undefined
            return undefined;
        }
    }

    async deleteSubscription(roomId: RoomId): Promise<void> {
        await this.subscriptions.delete(this.hash(roomId));
        await this.maxStatusIds.delete(roomId);
        await this.maxNotificationIds.delete(roomId);
        await this.keyv.delete(this.ongoingRegistrationKey(roomId));
    }

    // -- Ongoing registrations --

    private ongoingRegistrationKey(roomId: RoomId): string {
        return `ongoingRegistration:${this.hash(roomId)}`;
    }

    async addOngoingRegistration(registration: OngoingRegistration): Promise<void> {
        return this.trySet(this.ongoingRegistrationKey(registration.roomId), registration);
    }

    async getOngoingRegistration(roomId: RoomId): Promise<OngoingRegistration | undefined> {
        return this.tryGet(this.ongoingRegistrationKey(roomId));
    }

    async deleteOngoingRegistration(roomId: RoomId): Promise<void> {
        await this.keyv.delete(this.ongoingRegistrationKey(roomId));
    }
}

export interface FediverseConfigRepository {
    get(hostname: string): Promise<FediverseConfig | undefined>;
    add(fediverseConfig: FediverseConfig): Promise<void>;
}

export interface MaxIdRepository {
    get(roomId: RoomId): Promise<string | undefined>;
    set(roomId: RoomId, newValue: string): Promise<void>;
    delete(roomId: RoomId): Promise<void>;
}


