import CryptoJS from 'crypto-js';

export class Crypto {
    private secret: string;

    constructor(secret: string) {
        this.secret = secret;
    }

    encrypt(text: string): string {
        return CryptoJS.AES.encrypt(text, this.secret).toString()
    }

    decrypt(text: string): string {
        return CryptoJS.AES.decrypt(text, this.secret).toString(CryptoJS.enc.Utf8);
    }

    hash(text: string): string {
        return CryptoJS.SHA256(text).toString(CryptoJS.enc.Base64);
    }
}
