import megalodon from 'megalodon';
import { detector, Mastodon, MegalodonInterface, OAuth, Pleroma } from 'megalodon';
import { InstanceRef, SNS } from './types.ts';
import { extractFromError } from './error.ts';
import { Logger } from "@logtape/logtape";

// Workaround for https://questions.deno.com/m/1251216520202289182
const generator = megalodon.default

export interface FediverseConfig {
    ref: InstanceRef;
    clientId: string;
    clientSecret: string;
}

export interface SourceClient<Client extends MegalodonInterface> {
    client: Client;
    config: FediverseConfig;
}

export function initFediverseClient(config: FediverseConfig): SourceClient<MegalodonInterface> {
    const client = generator(config.ref.sns, `https://${config.ref.hostname}`);
    return { client, config };
}

export function isMastodon(client: SourceClient<MegalodonInterface>): client is SourceClient<Mastodon> {
    return client.config.ref.sns == 'mastodon';
}

export function isPleroma(client: SourceClient<MegalodonInterface>): client is SourceClient<Pleroma> {
    return client.config.ref.sns == 'pleroma';
}

export function initSubscriptionClient(config: InstanceRef, accessToken: string): MegalodonInterface {
    return generator(config.sns, `https://${config.hostname}`, accessToken);
}

export async function createFediverseApp(logger: Logger, url: URL, botAppName: string): Promise<FediverseConfig> {
    logger.debug(`Creating fediverse app for ${url.hostname}...`);
    const sns: SNS = await detector(`https://${url.hostname}`)
        .catch(error => {
            logger.debug(`Failed detecting SNS for ${url.hostname}: ${error}`);
            // Fallback to mastodon as the most supported API
            return 'mastodon';
        });
    const regClient = generator(sns, `https://${url.hostname}`);
    const appData: OAuth.AppData = await regClient.createApp(botAppName, {
        scopes: ['read'],
        redirect_uris: 'urn:ietf:wg:oauth:2.0:oob',
    });
    return {
        ref: { sns, hostname: url.hostname },
        clientId: appData.client_id,
        clientSecret: appData.client_secret
    };
}

// Check if the error is an error response from the server.
// If yes, return this error.
export function extractResponseError(error: unknown): string|undefined {
    const inner = extractFromError(error, 'response', 'data', 'error');
    return inner !== undefined ? `${inner}` : undefined;
}
