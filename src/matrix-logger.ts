import { ILogger } from 'matrix-bot-sdk';
import { Logger } from "@logtape/logtape";

// Disabling "no-explicit-any" rule here because it's used in an external interface.
/* eslint-disable @typescript-eslint/no-explicit-any */

export class MatrixLogger implements ILogger {
    inner: Logger;

    constructor(inner: Logger) {
        this.inner = inner;
    }

    info(module: string, ...messageOrObject: any[]) {
        this.inner.getChild(module).info(...messageOrObject);
    }
    warn(module: string, ...messageOrObject: any[]) {
        this.inner.getChild(module).warn(...messageOrObject);
    }
    error(module: string, ...messageOrObject: any[]) {
        this.inner.getChild(module).error(...messageOrObject);
    }
    debug(module: string, ...messageOrObject: any[]) {
        this.inner.getChild(module).debug(...messageOrObject);
    }
    trace(module: string, ...messageOrObject: any[]) {
        this.inner.getChild(module).debug(...messageOrObject);
    }
}