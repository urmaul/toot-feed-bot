import { InstanceRef, RoomId } from './types.ts';

export interface OngoingRegistration {
    // Matrix room id
    roomId: RoomId;
    // Fediverse instance ref
    instanceRef: InstanceRef,
}
