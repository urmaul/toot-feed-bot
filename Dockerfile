FROM denoland/deno:alpine-2.1.5

RUN apk add libstdc++

RUN mkdir /app
WORKDIR /app

ADD deno.* ./

RUN deno run install

ADD src/* ./src/
ADD .* ./

RUN deno cache src/main.ts

# Make sure tests pass
ADD test/* ./test/
RUN deno test
RUN rm -rf ./test/

ENTRYPOINT deno run --allow-env --allow-net --allow-sys --allow-ffi --allow-read --allow-write=/app/data/ src/main.ts
