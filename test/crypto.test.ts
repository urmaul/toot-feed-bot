'use strict';

import { describe, it } from "jsr:@std/testing/bdd";
import { expect } from "jsr:@std/expect";
import { Crypto } from '../src/crypto.ts';

// Use hardcoded ciphertexts to make sure updated code can work with old encrypted data.
describe('Crypto', () => {
    const secret = 'test_secret';
    const crypto = new Crypto(secret);

    it('should encrypt texts', () => {
        const input = 'test plaintext';
        const encrypted = crypto.encrypt(input);
        expect(encrypted).not.toEqual(input);
    });
    
    it('should encrypt texts', () => {
        const input = 'U2FsdGVkX1/HN59WkN4aq9CHguVurMlfUNe4u/keIhY=';
        const decrypted = crypto.decrypt(input);
        expect(decrypted).toEqual('test plaintext');
    });

    it('should hash texts', () => {
        const input = 'test plaintext';
        const hash = crypto.hash(input);
        expect(hash).toEqual('WJMTmAwqPR6m0qoN48yfhTejDVPSXSGS6R8j2M8Q0kU=');
    });
});
