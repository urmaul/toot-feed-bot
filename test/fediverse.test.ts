'use strict';

import { describe, it } from "jsr:@std/testing/bdd";
import { expect } from "jsr:@std/expect";
import { extractResponseError } from '../src/fediverse.ts';


describe('fediverse', () => {
    describe('extractResponseError', () => {
        it('extracts errors', () => {
            expect(extractResponseError('foo')).toEqual(undefined);
            expect(extractResponseError({})).toEqual(undefined);
            expect(extractResponseError({response:{data:42}})).toEqual(undefined);
            expect(extractResponseError({response:{data:{error:'oh hi'}}})).toEqual('oh hi');
            expect(extractResponseError({response:{data:{error:{message: 'oh hi'}}}})).toEqual('[object Object]');
        });
    });
});
